import requests
import json
import traceback
import qrcode
from kivy.network.urlrequest import UrlRequest
from kivy.app import App

class Firebase():
	web_api_key = "AIzaSyDt9L5jbu_e9oGuy_jgDMCpw9d1tx00H2o"
	
	global_data = ""

	Domicilio = ""
	amigos = ""
	edad = ""
	email = ""
	id_de_amigo = ""
	info_adic = ""
	nombre = ""
	tlf_contacto = ""
	ultima_pos = ""

	local_id = ""
	id_token = ""

	# Esta función se llama al empezar la app y cada vez que se hace un login
	# Con el refresh token se intenta loguear, al empezar la app se logueará automaticamente el último que estuviese
	# True: Ha salido bien ; False: Ha salido mal
	def on_start(self):
		try:
			with open("refreshToken.txt", 'r') as f:
				refresh_token = f.read()
				id_token, local_id = self.exchange_refresh_token(refresh_token) # Usamos el refresh_token para obtener un nuevo idToken

				## Obtener los datos de la BBDD
				result = requests.get("https://pti-bd.firebaseio.com/" + local_id + ".json?auth=" + id_token) # Recogemos los datos con el local_id del usuario
				data = json.loads(result.content.decode()) # Datos del usuario que se loguea
				self.global_data = data
				self.Domicilio = data['Domicilio']
				self.amigos = data['amigos'] 
				self.edad =	data['edad']
				self.email = data['email']
				self.id_de_amigo = data['id_de_amigo']
				self.info_adic = data['info_adic']
				self.nombre = data['nombre']
				self.tlf_contacto = data['tlf_contacto']
				self.ultima_pos = data['ultima_pos']
				self.local_id = local_id
				self.id_token = id_token

				return True		
		except:
			traceback.print_exc()
			return False

	# Registro de usuario, recibe el email y el password del usuario a registrar.
	# True: si se registra ; False: si ha habido algun error.
	def Signup(self, email, password):
		signup_url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/signupNewUser?key=" + self.web_api_key
		signup_payload = {"email" : email, "password" : password, "returnSecureToken" : True}
		signup_request = requests.post(signup_url, data=signup_payload)
		sign_up_data = json.loads(signup_request.content.decode())
		if signup_request.ok == True: # Todo ha ido bien
			refreshToken = sign_up_data["refreshToken"]
			localId = sign_up_data["localId"]
			idToken = sign_up_data["idToken"]
			
			with open("refreshToken.txt", "w") as f:
				f.write(refreshToken) # Guardamos el refreshToken en un archivo

			my_data = '{"Domicilio":"", "amigos":"", "edad":"", "email":"%s","id_de_amigo":"", "info_adic":"", "nombre":"", "tlf_contacto":"", "ultima_pos":""}' % email # Datos de nuevo usuario
			post_request = requests.patch("https://pti-bd.firebaseio.com/" + localId + ".json?auth=" + idToken, data=my_data) # Petición a la BBDD

		if signup_request.ok  == False: # No ha podido hacerse el registro
			error_data = json.loads(signup_request.content.decode())
			error_message = error_data["error"]["message"]
			print(error_message)

		return signup_request.ok

	# Log out, y ya no se restaura esa sesión	
	def Signout(self):
		with open("refreshToken.txt", 'w') as f:
			f.write("")

	# Da acceso a un usuario a la aplicación
	# True: execution ok ; False: Login error
	def Login(self, email, password):
		signin_url = "https://www.googleapis.com/identitytoolkit/v3/relyingparty/verifyPassword?key=" + self.web_api_key # Conecta con firebase
		signin_payload = {"email": email, "password": password, "returnSecureToken": True} # Crea datos para verificar
		signin_request = requests.post(signin_url, data=signin_payload) # Envia la petición de registro
		sign_up_data = json.loads(signin_request.content.decode())

		if signin_request.ok == True:
			refresh_token = sign_up_data['refreshToken']
			## Ahora guardaremos el refresh token en un archivo
			with open("refreshToken.txt", "w") as f:
				f.write(refresh_token)
			self.on_start() # LLama a onstart para recoger todos los datos y autenticar

		elif signin_request.ok == False:
			error_data = json.loads(signin_request.content.decode())
			print (error_data) # Error

		return signin_request.ok

	# Función para ir renovando la sesión
	def exchange_refresh_token(self, refresh_token):
		refresh_url = "https://securetoken.googleapis.com/v1/token?key=" + self.web_api_key ## Hay que renovar la sesión porque el idToken expira en una hora
		refresh_payload = '{"grant_type": "refresh_token", "refresh_token": "%s"}' % refresh_token
		refresh_req = requests.post(refresh_url, data=refresh_payload)
		id_token = refresh_req.json()['id_token']
		local_id = refresh_req.json()['user_id']
		return id_token, local_id

	# Devuelve la información solicitada del usuario logueado
	def get_domicilio(self):
		return self.Domicilio

	def get_edad(self):
		return self.edad

	def get_email(self):
		return self.email

	def get_info(self):
		return self.info_adic

	def get_nombre(self):
		return self.nombre

	def get_tlf_contacto(self):
		return self.tlf_contacto

	def get_ultima_pos(self):
		return self.ultima_pos

	def get_amigos(self):
		return self.amigos
		
	def get_data(self):
		return self.global_data

	# Añade un amigo
	# True: éxito ;  False: error
	def add_friend(self, friend_id):
		friend_id = friend_id.replace("\n","") # Quitamos el salto de línea
		check_req = requests.get('https://pti-bd.firebaseio.com/.json?orderBy="id_de_amigo"&equalTo=' + friend_id) # Comprobamos que existe el amigo
		data = check_req.json() # Si existe cogemos sus datos
		if data == {}:
			print("No existe un usuario con ese ID de amigo")
			return False
		if friend_id == self.id_de_amigo:
			print("No te puedes añadir a ti mismo")
			return False
		if friend_id in self.amigos.split(","):
			print("Ya has agregado a ese amigo")
			return False
		else:
			key = list(data)[0] # Cogemos el identificador del usuario
			new_friend_id = data[key]['id_de_amigo'] # Cogemos su ID de amigo
			if self.amigos == "":
				self.amigos += "%s" % friend_id
			else:
				self.amigos += ",%s" % friend_id
			patch_data =  '{"amigos" : "%s"}' % self.amigos
			patch_req = requests.patch("https://pti-bd.firebaseio.com/%s.json?auth=%s" % (self.local_id, self.id_token), data = patch_data) # Modificamos el campo de amigos añadiendo el nuevo

		return True

	# Elimina un amigo
	def remove_friend(self, friend_id_remove):
		self.amigos = self.amigos.replace(",%s"%friend_id_remove, "") # Borra uno de la lista
		self.amigos = self.amigos.replace("%s,"%friend_id_remove, "") # Borra el primero cuando hay varios
		if ',' not in self.amigos: # Sólo tiene un amigo
			if friend_id_remove==self.amigos:
				self.amigos = ""
		patch_data =  '{"amigos" : "%s"}' % self.amigos
		patch_req = requests.patch("https://pti-bd.firebaseio.com/%s.json?auth=%s" % (self.local_id, self.id_token), data = patch_data)


	# Asigna nuevo valor a los atributos
	def set_domicilio(self, new_domicilio):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (new_domicilio, self.amigos, self.edad, self.email, self.id_de_amigo, self.info_adic, self.nombre, self.tlf_contacto, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		
		if post_request.ok  == True:
			self.Domicilio = new_domicilio

		return post_request.ok


	def set_edad(self, new_edad):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, new_edad, self.email, self.id_de_amigo, self.info_adic, self.nombre, self.tlf_contacto, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		
		if post_request.ok  == True: # Si todo sale bien 
			self.edad = new_edad
			
		return post_request.ok


	def set_email(self, new_email):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, self.edad, new_email, self.id_de_amigo, self.info_adic, self.nombre, self.tlf_contacto, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		if post_request.ok  == True: # Si todo sale bien 
			self.email = new_email

		return post_request.ok


	def set_info(self, new_info):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, self.edad, self.email, self.id_de_amigo, new_info, self.nombre, self.tlf_contacto, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		if post_request.ok  == True: # Si todo sale bien 
			self.info_adic = new_info
			
		return post_request.ok


	def set_nombre(self, new_nombre):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, self.edad, self.email, self.id_de_amigo, self.info_adic, new_nombre, self.tlf_contacto, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		if post_request.ok  == True: # Si todo sale bien 
			self.nombre = new_nombre

		return post_request.ok


	def set_tlf_contacto(self, new_tlf):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, self.edad, self.email, self.id_de_amigo, self.info_adic, self.nombre, new_tlf, self.ultima_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		if post_request.ok  == True: # Si todo sale bien  
			self.tlf_contacto = new_tlf
			
		return post_request.ok


	def set_ultima_pos(self, new_pos):
		my_data = '{"Domicilio":"%s", "amigos":"%s", "edad":"%s", "email":"%s","id_de_amigo":"%s", "info_adic":"%s", "nombre":"%s", "tlf_contacto":"%s", "ultima_pos":"%s"}' % (self.Domicilio, self.amigos, self.edad, self.email, self.id_de_amigo, self.info_adic, self.nombre, self.tlf_contacto, new_pos)
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=my_data)
		if post_request.ok  == True: # Si todo sale bien 
			self.ultima_pos = new_pos

		return post_request.ok


	def set_data(self, new_data):
		post_request = requests.patch("https://pti-bd.firebaseio.com/" + self.local_id + ".json?auth=" + self.id_token, data=new_data)
		if post_request.ok  == True: # Si todo sale bien  
			self.global_data = new_data
		
		return post_request.ok


	# Obtiene toda la información de un amigo con su Id de amigo
	# Devuelve False si hay algun error, si no, devuelve los datos
	def get_friend_byId(self, friend_id):
		friend_id = friend_id.replace("\n","") # Quitamos el salto de línea
		##Habria que chequear que existe el amigo
		check_req = requests.get('https://pti-bd.firebaseio.com/.json?orderBy="id_de_amigo"&equalTo=' + friend_id)
		data = check_req.json() # Si existe cogemos sus datos
		if data == {}:
			print("No existe un usuario con ese ID de amigo")
			return False
		else:
			key = list(data)[0] # Cogemos el identificador del usuario
			return data[key]

	def gen_qr(self):
		qr = qrcode.QRCode(
			version=1,
			error_correction=qrcode.constants.ERROR_CORRECT_L,
			box_size=10,
			border=4,
			)
		data = "Nombre: %s\nEdad: %s\nDomicilio: %s\nInformación: %s"%(self.nombre, self.edad, self.Domicilio, self.info_adic)
		qr.add_data(data)
		qr.make(fit=True)

		img = qr.make_image(fill_color="black", back_color="white")
		img.save("codigo_qr_kivy")
